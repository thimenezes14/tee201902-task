import { Component } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-camera',
  templateUrl: './camera.page.html',
  styleUrls: ['./camera.page.scss'],
})
export class CameraPage {

  imagem: string;

  options: CameraOptions = {
    quality: 20,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    correctOrientation: true
  };

  

  constructor(private camera: Camera) { }

  tiraFoto() {
    this.camera.getPicture(this.options)
      .then(imgData => {
        const base64Image = 'data:image/jpeg;base64,' + imgData;
        this.imagem = base64Image;
      });
  }

}
