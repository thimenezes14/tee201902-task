import { Task } from './../interfaces/task';
import { FakeDataService } from './fake-data.service';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class TaskService {
    taskList: Task[];

    constructor(
        // acesso ao banco de dados
        private data: FakeDataService
    ) {
        this.taskList = data.get();
    }

}
