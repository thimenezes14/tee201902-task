import { Task } from './../interfaces/task';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class FakeDataService {
    private data: Task[] = [
        {
            id: 1,
            title: 'Lavar carro',
            steps: [
                {
                    title: 'Pegar o sabão',
                    checked: false
                },
                {
                    title: 'Pegar o balde',
                    checked: false
                },
                {
                    title: 'Pegar a mangueira',
                    checked: false
                }
            ]
        },
        {
            id: 2,
            title: 'Arrumar a lâmpada do quarto',
            steps: [
                {
                    title: 'Comprar a lâmpada',
                    checked: false
                },
                {
                    title: 'Colocar a lampada',
                    checked: false
                }
            ]
        },
        {
            id: 3,
            title: 'Dormir',
            steps: [{
                title: 'Deitar',
                checked: false
            }]
        }
    ];

    constructor() { }

    public get() {
        return this.data;
    }

}
