import { Step } from './../interfaces/step';
import { FakeDataService } from './fake-data.service';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class StepService {
    stepList: Step[];

    constructor(
        private data: FakeDataService
    ) {}

    // arrow function
    getStepList(id: number) {
        const v = this.data.get();
        const s = v.filter(task => task.id === id);
        return s[0].steps;
    }

    markStep(step: Step) {
        step.checked = !step.checked;
        // save();
    }
}
