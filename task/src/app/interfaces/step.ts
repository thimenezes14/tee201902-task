
/**
 * Interface que representa os passos
 * da execução de uma tarefa.
 */
export interface Step {
    title: string;
    checked: boolean;
}
