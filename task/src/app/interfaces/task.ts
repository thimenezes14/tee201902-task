import { Step } from './step';

/**
 * Interface que representa uma tarefa do sistema.
 */
export interface Task {
    id: number;
    title: string;
    steps: Step[];
}
