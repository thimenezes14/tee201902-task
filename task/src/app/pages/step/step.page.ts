import { StepService } from './../../services/step.service';
import { Step } from './../../interfaces/step';
import { ActivatedRoute } from '@angular/router';
import { Component } from '@angular/core';

@Component({
    selector: 'app-step',
    templateUrl: './step.page.html',
    styleUrls: ['./step.page.scss'],
})
export class StepPage {
    lista: Step[];

    constructor(
        private route: ActivatedRoute,
        private service: StepService
    ) { }

    // ionic life cicle
    ionViewWillEnter() {
        const id = this.route.snapshot.paramMap.get('id');
        this.lista = this.service.getStepList(parseInt(id, 10));
    }

    markStep(step) {
        // delegação
        this.service.markStep(step);
    }
}

