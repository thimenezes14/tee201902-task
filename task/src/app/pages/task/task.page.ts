import { AlertController } from '@ionic/angular';
import { TaskService } from './../../services/task.service';
import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/interfaces/task';

@Component({
    selector: 'app-task',
    templateUrl: './task.page.html',
    styleUrls: ['./task.page.scss'],
})
export class TaskPage implements OnInit {
    // arquivos *.page guardam a lógica de apresentação
    // outras operações lógicas são realizadas por serviços
    lista: Task[];

    constructor(
        private service: TaskService,
        private alert: AlertController
    ) { }

    ngOnInit() {
        this.lista = this.service.taskList;
    }

    async newTask() {
        const form = await this.alert.create({
            header: 'TEE - 2019/2',
            message: 'Digite o nome da tarefa',
            inputs: [{type: 'text', name: 'task'}],
            buttons: [
                {text: 'Cancelar'},
                {text: 'Salvar', handler: data => this.save(data)}
            ]
        });

        form.present();
    }

    private save(data) {
        const idt = this.lista.length + 1;
        const task: Task = {id: idt, title: data.task, steps: []};
        this.lista.push(task);
    }

}
