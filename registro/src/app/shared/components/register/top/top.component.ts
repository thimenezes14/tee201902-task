import { Component, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'top',
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.scss'],
})
export class TopComponent {

  @Output() 
  logoClicked: EventEmitter<any>;
  
  @Input() 
  title: String;
  
  @Input() 
  color: String;

  contador = 0;

  constructor() { 
    this.logoClicked = new EventEmitter();
  }

  imgClicked() {
    this.logoClicked.emit(++ this.contador); 
  }

}
