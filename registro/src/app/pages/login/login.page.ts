import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  db: any[];
  user: String;
  pass: String;
  formValid: boolean = false;

  constructor(private http: HttpClient, private nav: NavController) { }

  ngOnInit() {
    this.http.get('assets/config/login.json')
    .subscribe((res: any) => {
      this.db = res.data;
    });
  }

  validacao(text: String) {

    if(text.length < 8) {
      this.formValid = false;
    } else {
      this.formValid = true;
    }
    
  }

  login() {
    const obj = this.db.filter(x => x.user === this.user && x.pass === this.pass);
    if(obj.length > 0) {
      console.log("Deu certo. Login realizado");
      this.nav.navigateForward('dados-pessoais');
    } else {
      console.log("Erro ao realizar login. ");
    }
  }

  report(event) {
    console.log("A imagem foi clicada " + event + " vezes. ");
  }

}
