import { Component, OnInit } from '@angular/core';
import { AlertController, IonItemSliding } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

  //Lista de testes
  /*lista = [
    {nome: 'Thiago', email: 'thi@mail.com', idade: '20'},
    {nome: 'Bruna', email: 'bru@mail.com', idade: '18'},
  ]*/

  lista: Observable<any[]>;

  constructor(private alert: AlertController, private db: AngularFirestore) { }

  ngOnInit() {
    this.lista = this.db.collection('usuarios').valueChanges({idField: 'id'});
  }

  async criarUsuario(usuario?: any) {  
    const alert = await this.alert.create({
      subHeader: 'Digite os dados do usuário',
      inputs: [
        {type: 'text', name: 'nome', placeholder: 'Nome', value: usuario ? usuario.nome : ''},
        {type: 'email', name: 'email', placeholder: 'E-mail', value: usuario ? usuario.email : '' },
        {type: 'number', name: 'idade', placeholder: 'Idade', value: usuario ? usuario.idade : ''}
      ],
      buttons: [
        {text: 'Cancelar', role: 'cancel'},
        {text: 'Enviar', handler: data => this.enviarDados(data, usuario)}
      ]
    });

    alert.present();
  }

  editar(usuario, item: IonItemSliding) {
    this.criarUsuario(usuario);
    item.close();
  }

  excluir(usuario, item: IonItemSliding) {
    this.db.doc(`usuarios/${usuario.id}`).delete();
    item.close();
  }

  private enviarDados(data, usuario) {
    usuario ? 
      this.db.doc(`usuarios/${usuario.id}`).update(data) : 
      this.db.collection('usuarios').add(data)
      .then(()=> console.log("Tudo certo! Usuário criado!"))
      .catch(err => console.log(`Erro: ${err}`));
  }

}
